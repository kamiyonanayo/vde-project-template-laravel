#!/usr/bin/env bash

set -u

# LOG_PATH="/var/log/nginx/access_pipe.log"
# OUT_LOG_DIR="/var/log/nginx"
# OUT_LOG_FILE_NAME="access_pipe.log.%Y-%m-%d.%H.%M"
# /usr/bin/env cat "$LOG_PATH" | /usr/bin/env cronolog --period=30minutes "$OUT_LOG_DIR/$OUT_LOG_FILE_NAME"

LOG_PATH="$1"
shift;

/usr/bin/env cat "$LOG_PATH" | tee >(/usr/bin/env cronolog "$@")
