#!/bin/bash

set -u;

function is_php_apache(){
  if command -v apache2-foreground > /dev/null; then
    echo 0;
  else
    echo 1;
  fi
}

function get_role(){
  local role=${1:-app}

  if [[ "$role" == "apache2" ]] || [[ "$role" == "php-fpm" ]] || [[ "$role" != "app" ]]; then
    printf "$role"
  elif [[ "$(is_php_apache)" -eq 0 ]]; then
    printf "apache2"
  else
    printf "php-fpm"
  fi
}

function get_docker_signal(){
  local signal=${1:-}

  if [[ -n "$signal" ]]; then
    printf "$signal"
  elif [[ "$(is_php_apache)" -eq 0 ]]; then
    printf 'WINCH'
  else
    printf 'SIGQUIT'
  fi
}

declare -r RUN_ROLE="$(get_role "${CONTAINER_ROLE:-}")"
declare -r RUN_USER="$(get_role "${CONTAINER_RUN_USER:-www-data}")"
declare -r DOC_ROOT="${DOCUMENT_ROOT:-/var/www/html}"
declare -r LARAVEL_CACHE="${LARAVEL_INIT_CACHE:-}"
declare -r DOCKER_STOP_SIGNAL="$(get_docker_signal ${DOCKER_STOP_SIGNAL:-})"
declare -r RUN_STOP_SIGNAL="${CONTAINER_STOP_SIGNAL:-"SIGTERM"}"

declare -a pid_list=()

function main(){

  if [ "$RUN_ROLE" = "apache2" ]; then

    cache_configuration
    exec_apache "$@"

  elif [ "$RUN_ROLE" = "php-fpm" ]; then

    cache_configuration
    exec_php_fpm "$@"

  elif [ "$RUN_ROLE" = "queue" ]; then

    exec_queue "$@"

  elif [ "$RUN_ROLE" = "scheduler" ]; then

    exec_scheduler "$@"

  elif [ "$RUN_ROLE" = "exec" ]; then

    exec_exec "$@"

  else
    errmsg "Could not match the container role \"$RUN_ROLE\""
    exit 1
  fi

}

function exec_apache(){
  exec apache2-foreground
}

function exec_php_fpm(){
  exec php-fpm
}

function exec_queue(){
  msg "Running the queue..."

  if [[ ! -f "$DOC_ROOT/../artisan" ]]; then
    errmsg "artisan not exists: $DOC_ROOT/../artisan"
    exit 1
  fi

  trap "queue_kill_signal_handler $DOCKER_STOP_SIGNAL" "$DOCKER_STOP_SIGNAL"

  local op=${CONTAINER_OPTIONS:-"--verbose --tries=3 --timeout=90"}
  exec_suexec php "$DOC_ROOT/../artisan" queue:work $op &
  local pid="$!"

  while [ true ]
  do
    sleep 1
    if ! ps -p "$pid" > /dev/null ; then
      errmsg "queue worker process not exists:pid=$pid"
      exit 1
    fi
  done
}

function queue_kill_signal_handler() {
    exec php "$DOC_ROOT/../artisan" queue:restart
}

function exec_scheduler(){

  msg "Running the scheduler..."

  if [[ ! -f "$DOC_ROOT/../artisan" ]]; then
    errmsg "artisan not exists: $DOC_ROOT/../artisan"
    exit 1
  fi

  trap "scheduler_kill_signal_handler $DOCKER_STOP_SIGNAL" "$DOCKER_STOP_SIGNAL"

  local op=${CONTAINER_OPTIONS:-"--verbose --no-interaction"}

  scheduler_sleep_wait $(date "+%S")

  while [ true ]
  do

    msg "run schedule"
    exec_suexec php "$DOC_ROOT/../artisan" schedule:run $op &
    local pid="$!"
    local sleep_sec=$(date "+%S")

    pid_list=("${pid_list[@]}" $pid)
    scheduler_clean_pid_list

    scheduler_sleep_wait $sleep_sec

  done

}

function scheduler_sleep_wait(){

  local sec=$1
  sec=$((10#$sec))
  sec=$((60 - ( $sec % 60 )))

  #sleep $sec &
  #wait $!

  local limit=$(date --date "$sec seconds" "+%s")

  while (( $(date "+%s") < $limit ))
  do
    sleep 1
  done

}

function scheduler_kill_signal_handler() {
  local i=0
  local ret=0
  for pid in ${pid_list[@]}; do
    if kill -0 $pid > /dev/null 2>&1; then
      kill -s "$RUN_STOP_SIGNAL" "$pid" &
      wait $pid
      pid_ret=$?
      if (( $pid_ret )); then
        errmsg "exit error php exit code:$pid_ret"
        ret=$pid_ret
      fi
    fi
    let i++
  done

  exit $ret

}

function scheduler_clean_pid_list(){

  local new_pid_list=()

  local i=0
  for pid in ${pid_list[@]}; do
    if kill -0 $pid > /dev/null 2>&1; then
      new_pid_list=("${new_pid_list[@]}" $pid)
    fi
  done

  pid_list=("${new_pid_list[@]}")

}

function exec_exec(){
  local command="$CONTAINER_OPTIONS"

  exec_suexec bash -c "$command" &
  pid="$!"

  trap "exec_kill_signal_handler $pid" "$DOCKER_STOP_SIGNAL"

  while [ true ]
  do
    sleep 1
    if ! ps -p "$pid" > /dev/null ; then
      errmsg "exec process not exists:pid=$pid,command=$command"
      exit 1
    fi
  done
}

function exec_kill_signal_handler() {
  local pid="$1"
  exec kill -s "$RUN_STOP_SIGNAL" "$pid"
}

function cache_configuration(){

  if [ "$LARAVEL_CACHE" != "" ]; then
    msg "Caching configuration..."
    (cd "$DOC_ROOT/.." && composer dump-autoload)
    (cd "$DOC_ROOT/.." && php artisan config:cache)
    (cd "$DOC_ROOT/.." && php artisan route:cache)
    (cd "$DOC_ROOT/.." && php artisan view:cache)
  fi

}

function exec_suexec(){
  gosu "$RUN_USER" "$@"
}

function msg(){
  echo "$@" > /dev/stdout
}

function errmsg(){
  echo "$@" > /dev/stderr
}

main "$@"
