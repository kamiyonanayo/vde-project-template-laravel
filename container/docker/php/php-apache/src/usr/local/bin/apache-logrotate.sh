#!/bin/sh

#LOG_PATH=/var/log/apache2
#LOG_PATH=/usr/local/apache2/logs
if [ $# -le 0 ]; then
    echo "args error.$#"
    exit 1
fi

LOG_PATH="$1"
INTERVAL=5M
NUMBEROFFILES=4

/usr/bin/env rotatelogs -f -l -n "${NUMBEROFFILES}" "${LOG_PATH}" "${INTERVAL}"
