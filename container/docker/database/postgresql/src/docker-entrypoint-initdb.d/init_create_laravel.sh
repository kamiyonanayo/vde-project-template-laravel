#!/bin/bash

set -eux

CMD_SCRIPT="${0}"
CMD_NAME="`basename $CMD_SCRIPT`"

psql --variable=ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
  CREATE ROLE laravel WITH LOGIN PASSWORD 'laravel';
  CREATE DATABASE laravel WITH OWNER laravel;
EOSQL
