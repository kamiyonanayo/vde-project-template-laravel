#!/bin/bash

CMD_SCRIPT="${0}"
CMD_NAME="`basename $CMD_SCRIPT`"

num=1

eval databse=\"\${MYSQL_INIT_${num}_DATABASE:-}\"
eval user=\"\${MYSQL_INIT_${num}_USER:-}\"
eval pass=\"\${MYSQL_INIT_${num}_PASSWORD:-}\"

while [ -n "${databse}" ] || [ -n "${user}" ] || [ -n "${pass}" ]
do

  if [ -n "${databse}" ]; then
    mysql_note "Creating database ${databse}"
    docker_process_sql --database=mysql <<<"CREATE DATABASE IF NOT EXISTS \`${databse}\` ;"
  fi

  if [ -n "${user}" ] && [ -n "${pass}" ]; then
      mysql_note "Creating user ${user}"
      docker_process_sql --database=mysql <<<"CREATE USER '${user}'@'%' IDENTIFIED BY '${pass}' ;"

      if [ -n "${databse}" ]; then
        mysql_note "Giving user ${user} access to schema ${databse}"
        docker_process_sql --database=mysql <<<"GRANT ALL ON \`${databse//_/\\_}\`.* TO '${user}'@'%' ;"
      fi
  else
    if [ -n "${MYSQL_USER:-}" ] && [ -n "${MYSQL_PASSWORD:-}" ] && [ -n "${databse}" ]; then
      # MYSQL_USER=app_user MYSQL_PASSWORD=app_pass MYSQL_INIT_1_DATABASE=test01 MYSQL_INIT_1_USER= MYSQL_INIT_1_PASSWORD=
      # => GRANT ALL ON `app_user`.* TO 'test01'@'%' ;
      mysql_note "Giving user ${MYSQL_USER} access to schema ${databse}"
      docker_process_sql --database=mysql <<<"GRANT ALL ON \`${databse//_/\\_}\`.* TO '${MYSQL_USER}'@'%' ;"
    fi
  fi

  num=$(($num+1))
  eval databse=\"\${MYSQL_INIT_${num}_DATABASE:-}\"
  eval user=\"\${MYSQL_INIT_${num}_USER:-}\"
  eval pass=\"\${MYSQL_INIT_${num}_PASSWORD:-}\"

done

mysql_note "$CMD_SCRIPT finish"
