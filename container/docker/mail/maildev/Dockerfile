FROM alpine:3.18 as base

SHELL ["/bin/sh", "-oeux", "pipefail", "-c"]

ENV NODE_ENV production

RUN : \
    && addgroup -g 1000 node \
    && adduser -u 1000 -G node -s /bin/sh -D node \
    && :

FROM base as builder
RUN mkdir -p /tmp/app
WORKDIR /tmp/app

# ISO-2022-JP対応
RUN : \
    && apk add --update \
        gcc \
        make \
        g++ \
        python3 \
        git \
        nodejs \
        npm \
    && npm install -g node-gyp \
    && npm install maildev  \
    && npm install iconv \
    && npm prune \
    && :

FROM base as runner

RUN : \
    && apk add --no-cache \
        nodejs \
    && mkdir -p /usr/src/app \
    && :

USER node
WORKDIR /usr/src/app
COPY --chown=node:node --from=builder /tmp/app .

EXPOSE 80 1025
ENV MAILDEV_WEB_PORT 80
ENV MAILDEV_SMTP_PORT 1025

CMD ["node", "node_modules/maildev/bin/maildev", "--web", "80", "--smtp", "1025"]
