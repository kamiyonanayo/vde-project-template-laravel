#!/usr/bin/env bash

set -eux;

SCRIPT_DIR=$(cd $(dirname $0); pwd)

pushd $SCRIPT_DIR

docker compose down --remove-orphans

popd
