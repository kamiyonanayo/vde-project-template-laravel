@echo off

pushd "%~dp0"
set SCRIPT_DIR=%CD%

cd "%SCRIPT_DIR%\..\..\"
for %%I in (.) do set CurrDirName=%%~nxI

cd "%SCRIPT_DIR%\"

echo Exit (press Ctrl+C)

ssh dev_vm ^
      -q ^
      -F "%SCRIPT_DIR%\..\..\..\..\vagrant\env-host\vm_host.ssh_config" ^
      -t ^
      bash --login run_app_container.sh """%CurrDirName%""" docker-compose logs --follow


exit
