#!/usr/bin/env bash

set -ux;

SCRIPT_DIR=$(cd $(dirname $0); pwd)

pushd $SCRIPT_DIR

cd "../docker"

docker compose exec app bash -c "cd \"\$DOCUMENT_ROOT/../\" && npm install && npm run build -- --watch"

popd
