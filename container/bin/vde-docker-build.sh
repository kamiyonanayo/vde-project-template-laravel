#!/usr/bin/env bash

set -eux;

SCRIPT_DIR=$(cd $(dirname $0); pwd)

pushd $SCRIPT_DIR

cd "../docker"

docker compose build

popd
