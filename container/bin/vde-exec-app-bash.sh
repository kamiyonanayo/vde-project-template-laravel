#!/usr/bin/env bash

set -u;

SCRIPT_DIR=$(cd $(dirname $0); pwd)

pushd $SCRIPT_DIR

cd "../docker"

docker compose exec app bash

popd
