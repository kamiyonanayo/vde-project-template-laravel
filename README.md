
# Laravelテンプレートプロジェクト

## インストール手順


1. `container/docker/.env` の `COMPOSE_PROJECT_NAME` と `PROJECT_DOMAIN_NAME` を変更
2. プロジェクトを起動(`docker-compose-start.bat` or `cd container/docker && docker compose up --detach`)
3. src/www ディレクトリを削除
4. app内でコマンドを実行(`docker-compose-exec-app-bash.bat` or `cd container/docker && docker compose exec app bash`)
5. Laravel プロジェクト作成(`cd /data/src && composer create-project laravel/laravel www`)

### アクセスURL

* http://${PROJECT_DOMAIN_NAME}.ホスト名.domain/
* http://${PROJECT_DOMAIN_NAME}-mail.ホスト名.domain/
* http://${PROJECT_DOMAIN_NAME}-adminer.ホスト名.domain/

## その他

### PostgreSQL

PostgreSQLを使用する場合

#### `compose.yaml` のdbをserviceに変更する

```yaml
  # PostgreSQL
  db:
    build:
      context: ./database/postgresql
      dockerfile: "Dockerfile.16"
    environment:
      - TZ=Asia/Tokyo
      - POSTGRES_PASSWORD=pass
    volumes:
      # データ格納場所
      - postgresql:/var/lib/postgresql
    networks:
      - default
    mem_limit: 512m
```

#### .evnファイルを修正する

* `DB_CONNECTION` を `pgsql`に設定

```bash
DB_CONNECTION=pgsql
DB_HOST=db
DB_PORT=5432
DB_DATABASE=laravel
DB_USERNAME=laravel
DB_PASSWORD=laravel
```

### Vite

Viteを使用する場合

#### 1. npm run build(vite build --watch) を使用する場合

`container/cmd_tools/docker-compose-exec-app-npm-build.bat` を実行する

#### 2. npm run dev(vite) を使用する場合

##### 2.1. `compose.yaml` にserviceを追加する

```yaml
  app-vite:
    <<: *x-app
    environment:
      TZ: "Asia/Tokyo"
      DOCUMENT_ROOT: /data/src/www/public
      CONTAINER_ROLE: exec
      CONTAINER_OPTIONS: cd /data/src/www && npm install && npm run dev
      VIRTUAL_HOST: "${PROJECT_DOMAIN_NAME}-asset.${DEV_VM_HOST_HOSTNAME}.${DEV_VM_GUEST_DOMAIN:-dev.internal}"
      VIRTUAL_PORT: 5173
      HTTPS_METHOD: noredirect
    networks:
      - default
      - backend_nw
```

##### 2.2. vite.config.jsファイルを修正する

* `vite.config.js` に `server` の設定を追加する

```js
server: {
    host: true,
    hmr: {
        host: process.env.VIRTUAL_HOST,
        clientPort: 80,
    }
}
```

###### 2.2.1 vite.config.js の例

```js
import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/sass/app.scss',
                'resources/js/app.js',
            ],
            refresh: true,
        }),
    ],
    server: {
        host: true,
        hmr: {
            host: process.env.VIRTUAL_HOST,
            clientPort: 80,
        },
    },
});
```

### Redis

redisを使用する場合

#### `compose.yaml` にserviceを追加する

```yaml
  redis:
    build:
      context: ./database/redis
      dockerfile: "Dockerfile"
    command: redis-server --save 60 1 --loglevel notice
    volumes:
      # データ格納場所
      - ../storage/redis:/data
    networks:
      - default
```

#### Laravelのプロジェクトに Composer で predis を追加する

```bash
cd '/data/src/www'
composer require 'predis/predis'
```

#### .evnファイルを修正する

* `REDIS_CLIENT` を `predis` に設定
* `REDIS_HOST` を サービス名(`redis`)に設定

```bash
REDIS_CLIENT=predis
REDIS_HOST=redis
REDIS_PASSWORD=null
REDIS_PORT=6379
```

### memcached

memcachedを使用する場合


#### `compose.yaml` にserviceを追加する

```yaml
  memcached:
    build:
      context: ./database/memcached
      dockerfile: "Dockerfile"
    command: memcached -m 64
    networks:
      - default
```

#### .evnファイルを修正する

* `MEMCACHED_HOST` を サービス名(`memcached`)に設定

```bash
MEMCACHED_HOST=memcached
```
